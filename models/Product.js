const mongoose = require("mongoose");


const productSchema = new mongoose.Schema({
	name: {
		type: String,
		required: [true, "Name is required"]
	},
	funkoNumber:{
		type: String,
		required: [true, "Funko ID required"]
	},
	description: {
		type: String,
		required: [true, "description is required"]
	},
	price: {
		type: Number,
		required: [true, "Price is required"]
	},

	isActive: {
		type: Boolean,
		default: true
	},
	stock: {
		type: Number,
		default: 0
	},
	image: {
		type: String,
		default: ""
	},
	createdOn : {
		type: Date,
		default: new Date()
	}

});

module.exports = mongoose.model("Product", productSchema);
