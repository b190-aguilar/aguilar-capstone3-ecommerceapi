const express = require("express");
const router = express.Router();
const orderController = require("../controllers/orderController.js");
const auth = require("../auth.js");


// (USER function) route for creating an order
router.post('/checkout', auth.verify, (req,res)=>{
	const userData = auth.decode(req.headers.authorization);
	// console.log(userData);
	if(userData.isAdmin){
		res.send({auth: "Logged in as Admin, createOrder function not applicable"});
	}
	else{
		let data = {
			userId: userData.id,
			products: req.body.products,
			totalAmount: req.body.total
		}
		orderController.createOrder(data).then(resultFromController => res.send(resultFromController));
	};

});


// (USER function) route for checking orders
router.get('/myOrders', auth.verify, (req,res)=>{
	const userData = auth.decode(req.headers.authorization);
	// console.log(userData);
	if(userData.isAdmin){
		res.send({auth: "Logged in as Admin, myOrders function not applicable"});
	}
	else{
		orderController.myOrders(userData.id).then(resultFromController => res.send(resultFromController));
	}
});

// (ADMIN function) route for getting all orders
router.get('/allOrders', auth.verify, (req,res)=>{
	const userData = auth.decode(req.headers.authorization);
	if(userData.isAdmin){
		orderController.getAllOrders().then(resultFromController => res.send(resultFromController));
	}
	else{
		res.send({auth: "Unauthorised user"});
	}
});

	


module.exports = router;