const express = require("express");
const router = express.Router();
const userController = require("../controllers/userController.js");
const auth = require("../auth.js");

// route for user registration
router.post("/register", (req,res) =>{
	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController));
});

// route for login user
router.post("/login", (req,res) =>{
	userController.loginUser(req.body).then(resultFromController => res.send(resultFromController));
});

// route for user get profile
router.get("/profile", auth.verify, (req,res) =>{
	const userId = auth.decode(req.headers.authorization).id;
	// console.log(userId);
	userController.getProfile(userId).then(resultFromController => res.send(resultFromController));
});


// (ADMIN Function) route for user set to admin
router.put("/:userId/setAsAdmin", auth.verify, (req,res) =>{
	const userData = auth.decode(req.headers.authorization);
	if(userData.isAdmin === false){
		res.send({auth: "Unauthorised user"});
	}
	else{
		userController.setAsAdmin(req.params).then(resultFromController => res.send(resultFromController));
	};
	
});

// (ADMIN Function) route for removing as admin
router.put("/:userId/removeAdmin", auth.verify, (req,res) =>{
	const userData = auth.decode(req.headers.authorization);
	if(userData.isAdmin === false){
		res.send({auth: "Unauthorised user"});
	}
	else{
		userController.removeAdmin(req.params).then(resultFromController => res.send(resultFromController));
	};
	
});


// (ADMIN Function) route for getting all users
router.get('/all',(req,res)=>{
	const userData = auth.decode(req.headers.authorization);
		// console.log(userData);
	if(userData.isAdmin === false){
		res.send({auth: "Unauthorised user"});
	}
	else{
		userController.getAllUsers().then(resultFromController => res.send(resultFromController));
	};
});



module.exports = router;