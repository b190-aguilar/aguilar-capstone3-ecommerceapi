const express = require("express");
const router = express.Router();
const productController = require("../controllers/productController.js");
const auth = require("../auth.js");

// route for getting all products
router.get('/all',(req,res)=>{
	const userData = auth.decode(req.headers.authorization);
	productController.getAllProducts().then(resultFromController => res.send(resultFromController));	
});

// route for getting all active products
router.get('/allActive',(req,res)=>{
	productController.getAllActiveProducts().then(resultFromController => res.send(resultFromController));
});

// (ADMIN function) route to add product
router.post('/addProduct', auth.verify, (req,res)=>{
	const userData = auth.decode(req.headers.authorization);
	if(userData.isAdmin){
		productController.addProduct(req.body).then(resultFromController => res.send(resultFromController));
	}
	else{
		res.send({auth: "Unauthorised user"});
	}
});


// route to find specific product
router.get('/:productId', (req,res) =>{
	productController.getProduct(req.params).then(resultFromController => res.send(resultFromController));
});


// (ADMIN function) route to add product stock 
router.put('/:productId/updateStock', auth.verify, (req,res)=>{
	const userData = auth.decode(req.headers.authorization);
	console.log(req.body);
		productController.updateProductStock(req.body).then(resultFromController => res.send(resultFromController));

});

// (ADMIN function) route to update product price
router.put('/:productId/updatePrice', auth.verify, (req,res)=>{
	const userData = auth.decode(req.headers.authorization);
	console.log(req.params);
	console.log(req.body);
	if(userData.isAdmin){
		productController.updatePrice(req.params,req.body).then(resultFromController => res.send(resultFromController));
	}
	else{
		res.send({auth: "Unauthorised user"});
	};
});

// (ADMIN function) update a product

router.put('/:productId', (req,res)=>{
	productController.updateProduct(req.params,req.body).then(resultFromController => res.send(resultFromController));
});

// (ADMIN function) route to archive product
router.put('/:productId/archive', auth.verify, (req,res)=>{
	const userData = auth.decode(req.headers.authorization);
	if(userData.isAdmin){
		productController.archiveProduct(req.params,req.body).then(resultFromController => res.send(resultFromController));
	}
	else{
		res.send({auth: "Unauthorised user"});
	};
});


module.exports = router;


