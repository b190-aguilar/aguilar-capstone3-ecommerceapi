const Order = require("../models/Order.js");
const Product = require("../models/Product.js");
const User = require("../models/User.js");



// create order
module.exports.createOrder = async (data) =>{

	console.log(data);

// let isAvailable = await Product.findById(data.productId).then((product,error) =>{
// 		if(error){
// 				return false;
// 		}
// 		else{
// 			if(product.stock < 1){
// 				return false;
// 			}
// 			else{
// 				product.stock -= 1;
// 				total = product.price;
// 				return product.save().then((result,error) =>{
// 					if (error){
// 						return false;
// 					}
// 					else{
// 						return true;
// 					};
// 				});
// 			}
			
// 		};
// 	});

let cart = data.products;
console.log(`cart.length: ${cart.length}`);

let areProductsUpdated;
let item;

for(i=0;i<cart.length;i++){

	let stock = cart[i].stock-cart[i].quantity;
	let filter = {_id: cart[i]}

	console.log(`cart[${i}]: ${cart[i].id} ${cart[i].stock} ${cart[i].quantity}`);

	await Product.findOneAndUpdate({_id: cart[i].id}, {stock: stock}).then((product,error) =>{
		if(error){
			return false;
		}
		else{
			console.log(product);
			return (areProductsUpdated = true);
		}
	})
};


let mainUpdate = await User.findById(data.userId).then((user,error) => {
			if(error){
					return false;
			}
			else{
				let newOrder = new Order({
					userId: data.userId,
					products: data.products,
					totalAmount: data.totalAmount,
				});

				return newOrder.save().then((order, error) =>{
					if (error) {
						return false;
					}
					else{
						user.orders.push({orderId: order.id});
						return user.save().then((user,error) =>{
							if (error) {
								return false;
							}
							else {
								console.log(order);
								console.log(user);
								return true;
							};
						})
					};
				});	
			};
			
			});

		if(areProductsUpdated == true && mainUpdate == true){
			return {"Checkout": "Successful"};
		}
		else{
			return false;
		};
}

// Add to cart/order



// Checkout


// (ADMIN function) get allOrders 

module.exports.getAllOrders = () =>{
	return Order.find({}).then(result =>{
		return result;
	});
};


// (USER function) get myOrders
module.exports.myOrders = (userId) =>{
	return Order.find({userId: userId}).then((result) =>{
		return result;
	});
};
