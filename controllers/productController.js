const Product = require("../models/Product.js");

// get all products
module.exports.getAllProducts = () =>{
	return Product.find({}).then(result =>{
		return result;
	});
};

// get all products
module.exports.getAllActiveProducts = () =>{
	return Product.find({isActive: true},'name funkoNumber description price stock image').then(result =>{
		return result;
	});
};


// add product
module.exports.addProduct = (reqBody) =>{
	// console.log(reqBody);
	let newProduct = new Product({
			name: reqBody.name,
			funkoNumber: reqBody.funkoNumber,
			description: reqBody.description,
			price: reqBody.price,
			stock: reqBody.stock,
			image: reqBody.image
		});
	// check if product is already exists
	return Product.find({name: reqBody.name}).then(result =>{
		if(result.length > 0){
			let productId = result[0].id;
			return {result: false ,error: "Item already exists, please use add stock feature", productId};
		}
		else{
			return newProduct.save().then((product, error) =>{
				if (error) {
					return false;
				}else{
					return product;
				};
			});
		};
	});
};




// get specific product
module.exports.getProduct = (reqBody) =>{
	// console.log(reqBody);
	return Product.findById(reqBody.productId).then((result,error) =>{
		if(error){
			console.log(error);
			return false;
		}
		else {
			return result;	
		};
	});
};



// add stock
module.exports.updateProductStock = (product,reqBody) =>{
	console.log(product);
	console.log(reqBody);
	return Product.findById(product.productId).then((result,error) =>{
		if(error){
			console.log(error);
			return false;
		}
		else {
			console.log(result)
			result.stock = result.stock - reqBody.quantity;
			return result.save().then((product,error) =>{
				if(error){
					return false;
				}
				else{
					return product;
				}
			});
		};
	});
};

// update price
module.exports.updatePrice = (product,reqBody) =>{
	console.log(product);
	console.log(reqBody);
	return Product.findById(product.productId).then((result,error) =>{
		if(error){
			console.log(error);
			return false;
		}
		else {
			console.log(result)
			result.price = reqBody.price;
			return result.save().then((product,error) =>{
				if(error){
					return false;
				}
				else{
					return product;
				}
			});
		};
	});
};

// update product
module.exports.updateProduct = (reqParams,reqBody) =>{
	// console.log(reqParams);
	// console.log(reqBody);

	let updateProduct = {
		name: reqBody.name,
		funkoNumber: reqBody.funkoNumber,
		description: reqBody.description,
		price: reqBody.price,
		stock: reqBody.stock,
		image: reqBody.image,
	};
	// findByIdAndUpdate - its purpose is to find a specific id in the database using the 1st parameter, and update using the 2nd parameter
	/*
		.findByIdAndUpdate(documentId, updatesToBeApplied)
	*/
	return Product.findByIdAndUpdate(reqParams.productId, updateProduct ).then((result,error) => {
		if(error){
			console.log(error);
			return false;
		}
		else {
			return true;	
		};
	});
};


// archive product
module.exports.archiveProduct = (reqParams,reqBody) =>{
	// console.log(reqBody);c
	return Product.findById(reqParams.productId).then((result,error) =>{
		if(error){
			console.log(error);
			return false;
		}
		else {
			result.isActive = reqBody.isActive;
			return result.save().then((product, error) =>{
				if (error) {
					return false;
				}else{
					return true;
				};
			});	
		};
	});
};